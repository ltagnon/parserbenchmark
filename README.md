# ParserBenchmark

Benchmark destiné à évaluer et comparer les performances de différents parsers sur divers corpus linguistiques.

## Description

Le ParserBenchmark est conçu pour comparer divers parsers sur plusieurs corpus afin d'évaluer leur précision et performance. Le projet est structuré de manière à offrir une navigation intuitive, avec des dossiers dédiés pour chaque parser, les scripts d'évaluation, et les résultats.

## Installation et utilisation des parsers

### Spacy:

**Installation**:

pip install spacy
python -m spacy download fr_core_news_sm

Licence: MIT License. [Voir la licence complète](https://github.com/explosion/spaCy/blob/master/LICENSE)

### Stanza:

**Installation**:

pip install stanza

Licence: Apache License 2.0. [Voir la licence complète](https://github.com/stanfordnlp/stanza/blob/master/LICENSE)

### Trankit:

**Installation pour Python 3.8**:

pip install torch==1.8.1+cu111 torchvision==0.9.1+cu111 torchaudio===0.8.1 -f https://download.pytorch.org/whl/torch_stable.html
pip install trankit conllu
pip install numpy tqdm transformers adapter-transformers

Licence: Apache License 2.0. [Voir la licence complète](https://github.com/nlp-uoregon/trankit/blob/master/LICENSE)

### UDPipe:

**Installation**:

pip install ufal.udpipe

Après l'installation, téléchargez le modèle à partir du lien suivant: [Modèle UDPipe](https://ufal.mff.cuni.cz/udpipe/2/models).

Licence: MPL 2.0. [Voir la licence complète](https://github.com/ufal/udpipe/blob/master/LICENSE)

## Autres outils

### Matplotlib:

**Utilisation**:

Matplotlib a été utilisé pour la visualisation des données et la création de graphiques.

Licence: Basée sur la licence PSF, compatible BSD. [Voir la licence complète](https://matplotlib.org/stable/users/license.html)

### Seaborn:

**Utilisation**:

Seaborn a été utilisé pour améliorer la visualisation des données et notamment pour la création de cartes de chaleur.

Licence: BSD 3-Clause License. [Voir la licence complète](https://github.com/mwaskom/seaborn/blob/master/LICENSE.md)

### Scikit-learn (sklearn):

**Utilisation**:

Scikit-learn a été utilisé pour appliquer l'algorithme de clustering agglomératif et calculer le score de silhouette, afin d'évaluer la qualité de la division en clusters.

Licence: BSD 3-Clause License. [Voir la licence complète](https://github.com/scikit-learn/scikit-learn/blob/main/COPYING)

### NumPy:

**Utilisation**:

Numpy a été utilisé pour gérer les tableaux multidimensionnels et pour effectuer divers calculs mathématiques sur ces tableaux.

Licence: BSD 3-Clause License. [Voir la licence complète](https://github.com/numpy/numpy/blob/main/LICENSE.txt)

### Pandas:

**Utilisation**:

Pandas a été utilisé pour structurer les données sous forme de DataFrames et faciliter leur manipulation.

Licence: BSD 3-Clause License. [Voir la licence complète](https://github.com/pandas-dev/pandas/blob/main/LICENSE)

### Algorithme Zhang-Shasha (zss):

**Utilisation**:

L'algorithme Zhang-Shasha a été utilisé pour calculer la distance d'édition entre les arbres de dépendance générés par différents analyseurs.

Licence: BSD style License. [Voir la licence complète](https://github.com/timtadh/zhang-shasha/blob/master/LICENSE)

## Résultats

### Heatmap

Voici un exemple de heatmap produite pendant le benchmark :

![Heatmap GSD](./img/Heatmap_GSD.png)

### Scores LAS

Et voici le tableau des scores LAS produits à la fin du projet :

![Tableau LAS](./img/LAS.png)

Pour des résultats détaillés et complets, veuillez consulter le dossier "Résultats".

## Contribuer

Nous accueillons et apprécions les contributions de la communauté! Assurez-vous de lire les directives de contribution avant de soumettre une demande pull ou d'ouvrir une issue.

## Contact

Si vous avez des questions ou des préoccupations concernant le projet, ou si vous souhaitez en savoir plus sur la manière de contribuer, n'hésitez pas à nous contacter via mon adresse personelle: [ludovic.tgn@outlook.com](mailto:ludovic.tgn@outlook.com) ou via celle de mon maître de stage: [yannick.parmentier@loria.fr](mailto:yannick.parmentier@loria.fr)

## Licence

Ce projet est sous licence MIT. Voir le fichier [LICENSE](./LICENCE.md) pour plus de détails.

---

Merci d'utiliser ParserBenchmark et de contribuer à ma mission de fournir un benchmark de qualité pour les parsers linguistiques !


