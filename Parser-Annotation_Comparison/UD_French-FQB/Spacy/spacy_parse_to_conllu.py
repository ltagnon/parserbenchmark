import spacy
import os


def spacy_to_conllu(input_file, output_file):
    nlp = spacy.load("fr_core_news_sm")

    with open(input_file, 'r', encoding='utf-8') as infile, open(output_file, 'w', encoding='utf-8') as outfile:
        for line in infile:
            sentence = line.strip()
            if sentence:  # only process non-empty lines
                doc = nlp(sentence)

                outfile.write('# text = {}\n'.format(sentence))
                root_index = None
                modified_heads = {}  # To store modified head indices
                for token in doc:
                    if token.dep_ == 'ROOT':
                        if root_index is None:
                            root_index = token.i + 1
                        else:
                            modified_heads[token.i + 1] = root_index

                    # Use modified head if present, else use original head
                    head_index = modified_heads.get(token.i + 1, token.head.i + 1 if token.head != token else 0)

                    line_data = [
                        token.i + 1,  # ID
                        token.text,  # FORM
                        token.lemma_,  # LEMMA
                        token.pos_,  # UPOS
                        token.tag_,  # XPOS
                        '_',  # FEATS
                        head_index,  # HEAD
                        token.dep_,  # DEPREL
                        '_',  # DEPS
                        '_',  # MISC
                    ]
                    outfile.write('\t'.join(str(x) for x in line_data) + '\n')
                outfile.write('\n')


if __name__ == '__main__':
    script_dir = os.path.dirname(os.path.abspath(__file__))

    input_file = os.path.join(script_dir, 'fr_fqb-ud-test.txt')
    output_file = os.path.join(script_dir, 'AA.conll')

    spacy_to_conllu(input_file, output_file)
