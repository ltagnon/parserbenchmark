import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import silhouette_score

# Liste des parsers
parsers = ["Trankit", "Spacy", "Stanza", "UDPipe(gsd)", "UDPipe(sequoia)"]

# Matrice de distances
distances = np.array([
    [0, 3.589, 1.562, 8.060, 9.683],
    [3.589, 0, 3.325, 8.270, 9.554],
    [1.562, 3.325, 0, 7.972, 9.577],
    [8.060, 8.270, 7.972, 0, 9.512],
    [9.683, 9.554, 9.577, 9.512, 0]
])

# Définition du nombre de clusters à créer
n_clusters = 3

# Création du modèle de clustering
cluster = AgglomerativeClustering(n_clusters=n_clusters, affinity='precomputed', linkage='average')

# Application du modèle de clustering aux données
labels = cluster.fit_predict(distances)

# Calcul du score de silhouette pour l'organisation en clusters actuelle
silhouette = silhouette_score(distances, labels, metric='precomputed')

print(f"For n_clusters = {n_clusters}, the silhouette score is {silhouette}")

# Affichage des résultats du clustering
for parser, label in zip(parsers, labels):
    print(f"{parser} is in cluster {label}")

# Ajout des labels de cluster aux noms des parsers pour la visualisation
parsers_with_labels = [f"{parser} (Cluster {label})" for parser, label in zip(parsers, labels)]

# Création d'un DataFrame pour la visualisation
df = pd.DataFrame(distances, index=parsers_with_labels, columns=parsers_with_labels)

# Création de la carte de chaleur
plt.figure(figsize=(15, 10))
sns.heatmap(df, annot=True, cmap='coolwarm')
plt.title('Heatmap of dissimilarity scores')
plt.show()
