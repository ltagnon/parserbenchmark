import os
from conllu import parse_incr

def calculate_uas_and_las(gold_tree, parsed_tree):
    uas_correct = 0
    las_correct = 0
    total = 0

    for gold_token, parsed_token in zip(gold_tree, parsed_tree):
        if gold_token["head"] == parsed_token["head"]:
            uas_correct += 1
            if gold_token["deprel"] == parsed_token["deprel"]:
                las_correct += 1
        total += 1

    uas = uas_correct / total
    las = las_correct / total

    return uas, las

parsers_paths = {
    "Stanza-trained": "./predictions.conllu"
}

gold_path = "./fr_gsd-ud-test.conllu"

for parser, path in parsers_paths.items():
    with open(gold_path, 'r', encoding='utf-8') as gold_file, open(path, 'r', encoding='utf-8') as parsed_file:
        gold_data = parse_incr(gold_file)
        parsed_data = parse_incr(parsed_file)

        total_uas = 0
        total_las = 0
        num_sentences = 0

        for gold_tree, parsed_tree in zip(gold_data, parsed_data):
            uas, las = calculate_uas_and_las(gold_tree, parsed_tree)
            total_uas += uas
            total_las += las
            num_sentences += 1

        average_uas = total_uas / num_sentences
        average_las = total_las / num_sentences

        print(f"Pour le parser {parser}, le UAS moyen est {average_uas} et le LAS moyen est {average_las}")
