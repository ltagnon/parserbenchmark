import torch

device_index = torch.cuda.current_device()
device_name = torch.cuda.get_device_name(device_index)

print(f"Current device index: {device_index}")
print(f"Current device name: {device_name}")

