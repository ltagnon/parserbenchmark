import stanza

# Charge le modèle entraîné
nlp = stanza.Pipeline(lang='fr', processors={'depparse': 'default'}, dir='./saved_models/depparse')

# Charge le jeu de test
with open('fr_gsd-ud-test.conllu') as f:
    test_data = f.readlines()

# Ignore l'en-tête CoNLL-U
test_data = [line for line in test_data if not line.startswith("#")]

# Fait des prédictions sur le jeu de test
doc = nlp(''.join(test_data))

# Écrit les prédictions dans un fichier CoNLL-U
with open('predictions.conllu', 'w') as f:
    for sentence in doc.sentences:
        for word in sentence.words:
            f.write(f"{word.id}\t{word.text}\t{word.lemma}\t{word.upos}\t{word.xpos}\t{word.feats if word.feats else '_'}\t{word.head}\t{word.deprel}\t{word.deps if word.deps else '_'}\t{word.misc if word.misc else '_'}\n")
        f.write('\n')
