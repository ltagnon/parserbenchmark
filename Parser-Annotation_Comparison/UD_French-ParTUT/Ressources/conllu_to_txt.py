import os
from conllu import parse_incr

def conllu_to_raw_text(input_file, output_file):
    with open(input_file, 'r', encoding='utf-8') as infile, open(output_file, 'w', encoding='utf-8') as outfile:
        for tokenlist in parse_incr(infile):
            i = 0
            sentence = []
            while i < len(tokenlist):
                token = tokenlist[i]
                if isinstance(token['id'], tuple):
                    sentence.append(token['form'])
                    i += len(token['id'])
                else:
                    sentence.append(token['form'])
                    i += 1
            outfile.write(" ".join(sentence) + "\n")

if __name__ == '__main__':
    script_dir = os.path.dirname(os.path.abspath(__file__))

    input_file = os.path.join(script_dir, 'MA.conll')
    output_file = os.path.join(script_dir, 'MA.txt')

    conllu_to_raw_text(input_file, output_file)
