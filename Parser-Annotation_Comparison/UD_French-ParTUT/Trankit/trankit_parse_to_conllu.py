import os
import trankit
import torch
from conllu import TokenList

def raw_text_to_trankit(input_file, output_file):
    with open(input_file, 'r', encoding='utf-8') as infile, open(output_file, 'w', encoding='utf-8') as outfile:
        # Initialiser le parser Trankit pour le français
        p = trankit.Pipeline('french')

        for line in infile:
            text = line.strip()
            if not text:
                continue

            doc = p(text)

            for sentence in doc['sentences']:
                token_list = []
                for token in sentence['tokens']:
                    if 'expanded' in token and token['expanded'] is not None:
                        # This is a multiword token, handle it specially
                        multiword_data = {
                            "id": f"{token['expanded'][0]['id']}-{token['expanded'][-1]['id']}",
                            "form": token['text'],
                            "lemma": "_",
                            "upos": "_",
                            "xpos": "_",
                            "feats": "_",
                            "head": "_",
                            "deprel": "_",
                            "deps": "_",
                            "misc": "_"
                        }
                        token_list.append(multiword_data)
                        for subtoken in token['expanded']:
                            subtoken_data = {
                                "id": subtoken['id'],
                                "form": subtoken['text'],
                                "lemma": subtoken.get('lemma', '_'),
                                "upos": subtoken.get('upos', '_'),
                                "xpos": "_",
                                "feats": subtoken.get('feats', '_'),
                                "head": subtoken.get('head', 0),
                                "deprel": subtoken.get('deprel', '_'),
                                "deps": "_",
                                "misc": "_"
                            }
                            token_list.append(subtoken_data)
                    else:
                        token_data = {
                            "id": token['id'],
                            "form": token['text'],
                            "lemma": token.get('lemma', '_'),
                            "upos": token.get('upos', '_'),
                            "xpos": "_",
                            "feats": token.get('feats', '_'),
                            "head": token.get('head', 0),
                            "deprel": token.get('deprel', '_'),
                            "deps": "_",
                            "misc": "_"
                        }
                        token_list.append(token_data)
                conllu_sentence = TokenList(token_list).serialize()
                outfile.write(conllu_sentence)

if __name__ == '__main__':
    script_dir = os.path.dirname(os.path.abspath(__file__))

    input_file = os.path.join(script_dir, 'fr_partut-ud-dev.txt')
    output_file = os.path.join(script_dir, 'AA.conll')

    print("version de CUDA utilisée par PyTorch: " + torch.version.cuda)
    print("version de PyTorch: " + torch.__version__)

    raw_text_to_trankit(input_file, output_file)
