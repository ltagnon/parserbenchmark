import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import silhouette_score

# Liste des parsers
parsers = ["Trankit", "Spacy", "Stanza", "UDPipe(gsd)", "UDPipe(sequoia)"]

# Matrice de distances
distances = np.array([
    [0, 10.271, 13.7196, 26.2593, 19.8224],
    [10.271, 0, 17.3645, 25.9259, 20.2804],
    [13.7196, 17.3645, 0, 25.5185, 19.3578],
    [26.2593, 25.9259, 25.5185, 0, 26.4938],
    [19.8224, 20.2804, 19.3578, 26.4938, 0]
])

# Définition du nombre de clusters à créer
n_clusters = 2

# Création du modèle de clustering
cluster = AgglomerativeClustering(n_clusters=n_clusters, affinity='precomputed', linkage='average')

# Application du modèle de clustering aux données
labels = cluster.fit_predict(distances)

# Calcul du score de silhouette pour l'organisation en clusters actuelle
silhouette = silhouette_score(distances, labels, metric='precomputed')

print(f"For n_clusters = {n_clusters}, the silhouette score is {silhouette}")

# Affichage des résultats du clustering
for parser, label in zip(parsers, labels):
    print(f"{parser} is in cluster {label}")

# Ajout des labels de cluster aux noms des parsers pour la visualisation
parsers_with_labels = [f"{parser} (Cluster {label})" for parser, label in zip(parsers, labels)]

# Création d'un DataFrame pour la visualisation
df = pd.DataFrame(distances, index=parsers_with_labels, columns=parsers_with_labels)

# Création de la carte de chaleur
plt.figure(figsize=(15, 10))
sns.heatmap(df, annot=True, cmap='coolwarm')
plt.title('Heatmap of dissimilarity scores')
plt.show()
