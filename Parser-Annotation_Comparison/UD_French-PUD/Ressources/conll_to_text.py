# Importation des modules nécessaires
import re

# Chemins d'accès aux fichiers
chemin_fichier_source = 'MA.conll'  # Remplacez par votre fichier source
chemin_fichier_destination = 'destination.txt'  # Remplacez par votre fichier destination

# Ouverture du fichier source en mode lecture et du fichier destination en mode écriture
with open(chemin_fichier_source, 'r', encoding='utf-8') as fichier_source, open(chemin_fichier_destination, 'w', encoding='utf-8') as fichier_destination:

    # Parcours de chaque ligne du fichier source
    for ligne in fichier_source:

        # Si la ligne commence par "# text = "
        if ligne.startswith("# text = "):

            # Extraction de la phrase après "# text = " en utilisant l'expression régulière
            phrase = re.sub("# text = ", "", ligne)

            # Écriture de la phrase dans le fichier destination
            fichier_destination.write(phrase)

print("Extraction terminée.")

