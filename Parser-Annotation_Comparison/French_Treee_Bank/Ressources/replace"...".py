# Open the file in read mode and read its content
with open("MA.conll", "r", encoding="utf-8") as file:
    content = file.read()

# Replace all occurrences of "…" with "..."
content = content.replace("…", "...")

# Open the file in write mode and write the modified content
with open("MA.conll", "w", encoding="utf-8") as file:
    file.write(content)

print("Replacements made successfully.")

