import os
import zss
from conllu import parse

class Node:
    def __init__(self, label, children=None):
        self.label = label
        self.children = children or []

    def addkid(self, node, before=False):
        if before: self.children.insert(0, node)
        else: self.children.append(node)
        return self

def get_children(node):
    return node.children

def get_label(node):
    return node.label

def load_trees_from_conllu(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        data = file.read()
    sentences = parse(data)
    trees = [[(token['id'], token['head']) for token in sentence] for sentence in sentences]
    return trees

def adjac_to_nested_custom(adjac):
    node_dict = {node_id: Node(node_id) for node_id, _ in adjac}
    node_dict[0] = Node(0)
    for child_id, parent_id in adjac:
        if parent_id is None:
            continue
        node_dict[parent_id].addkid(node_dict[child_id])
    return node_dict[0]

parsers_paths = {
    "Trankit": "../Trankit/AA.conll",
    "Spacy": "../Spacy/AA.conll",
    "Stanza": "../Stanza/AA.conll",
    "UDPipe(gsd)": "../UDPipe(gsd)/AA.conll",
    "UDPipe(sequoia)": "../UDPipe(sequoia)/AA.conll"
}

parsers_nested_trees = {}

for parser, path in parsers_paths.items():
    auto_trees = load_trees_from_conllu(path)
    auto_nested = [adjac_to_nested_custom(tree) for tree in auto_trees]
    parsers_nested_trees[parser] = auto_nested

for parser1 in parsers_paths.keys():
    for parser2 in parsers_paths.keys():
        if parser1 == parser2:
            continue

        total_distance = 0
        num_pairs = min(len(parsers_nested_trees[parser1]), len(parsers_nested_trees[parser2]))

        for i in range(num_pairs):
            tree1 = parsers_nested_trees[parser1][i]
            tree2 = parsers_nested_trees[parser2][i]
            distance = zss.simple_distance(tree1, tree2, get_children, get_label)
            total_distance += distance

        average_distance = total_distance / num_pairs

        print(f"La distance moyenne entre les annotations de {parser1} et les annotations de {parser2} est : {average_distance}")
