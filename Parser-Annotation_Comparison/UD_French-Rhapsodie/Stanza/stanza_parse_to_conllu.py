import os
import stanza

stanza.download('fr', processors='tokenize')
from conllu import TokenList


def raw_text_to_conllu(input_file, output_file):
    with open(input_file, 'r', encoding='utf-8') as infile, open(output_file, 'w', encoding='utf-8') as outfile:
        nlp = stanza.Pipeline('fr', use_gpu=True)

        for line in infile:
            text = line.strip()
            if not text:
                continue

            doc = nlp(text)

            for sentence in doc.sentences:
                token_list = []
                for token in sentence.tokens:
                    if len(token.words) > 1:
                        # This is a multiword token, handle it specially
                        multiword_data = {
                            "id": f"{token.words[0].id}-{token.words[-1].id}",
                            "form": token.text,
                            "lemma": "_",
                            "upos": "_",
                            "xpos": "_",
                            "feats": "_",
                            "head": "_",
                            "deprel": "_",
                            "deps": "_",
                            "misc": "_"
                        }
                        token_list.append(multiword_data)
                    for word in token.words:
                        token_data = {
                            "id": word.id,
                            "form": word.text,
                            "lemma": word.lemma,
                            "upos": word.upos,
                            "xpos": word.xpos,
                            "feats": word.feats,
                            "head": word.head,
                            "deprel": word.deprel,
                            "deps": word.deps,
                            "misc": word.misc
                        }
                        token_list.append(token_data)
                conllu_sentence = TokenList(token_list).serialize()
                outfile.write(conllu_sentence)


if __name__ == '__main__':
    script_dir = os.path.dirname(os.path.abspath(__file__))

    input_file = os.path.join(script_dir, 'fr_rhapsodie-ud-dev.txt')
    output_file = os.path.join(script_dir, 'AA.conll')

    raw_text_to_conllu(input_file, output_file)
