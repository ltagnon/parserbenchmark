import os
import ufal.udpipe as udpipe
import sys


def parse_to_conllu(input_file, output_file, model_path):
    # Charger le modèle UDPipe
    model = udpipe.Model.load(model_path)
    if not model:
        sys.stderr.write("Erreur : impossible de charger le modèle UDPipe depuis le fichier {}\n".format(model_path))
        sys.exit(1)

    # Créer un pipeline avec le modèle
    pipeline = udpipe.Pipeline(model, 'tokenize', udpipe.Pipeline.DEFAULT, udpipe.Pipeline.DEFAULT, 'conllu')

    # Lire le fichier d'entrée
    with open(input_file, 'r', encoding='utf-8') as infile:
        text = infile.read()

    # Analyser le texte avec UDPipe
    conllu = pipeline.process(text)

    # Écrire le résultat dans un fichier CONLL-U
    with open(output_file, 'w', encoding='utf-8') as outfile:
        outfile.write(conllu)


if __name__ == '__main__':
    script_dir = os.path.dirname(os.path.abspath(__file__))

    input_file = os.path.join(script_dir, 'fr_rhapsodie-ud-dev.txt')
    output_file = os.path.join(script_dir, 'AA.conll')
    model_path = os.path.join(script_dir, 'french-gsd-ud-2.5-191206.udpipe')

    parse_to_conllu(input_file, output_file, model_path)
